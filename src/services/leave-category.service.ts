import { addDoc, collection, deleteDoc, doc, getDoc, getDocs, getFirestore, orderBy, query, serverTimestamp, setDoc, updateDoc } from "firebase/firestore"
import { LeaveCategory } from "../types/leave-category.type";

const db = getFirestore()


export async function createLeaveCategory(detail: string): Promise<any> {
    try {
        await addDoc(collection(db, "leave_categorys"), {
            detail: detail,
            timestamp: serverTimestamp()
        })
    } catch (error) {
        throw error;
    }
}

export async function getLeaveCategory() {
     const lcRef = collection(db, "leave_categorys")

     const q = query(lcRef, orderBy('timestamp', 'desc'))
     const docSnap = await getDocs(q)

     if (docSnap.empty) {
        return null
     }

     let lc: LeaveCategory[] = []
     docSnap.forEach((doc) => {
        let docTemp = doc.data() as LeaveCategory

        let lcTemp = {
            id: doc.id,
            detail: docTemp.detail
        }
        lc.push(lcTemp)
     })

     return lc

}

export async function getLeaveCategoryById(id: string) {
    const lcRef = doc(db, "leave_categorys", id)

    const docSnap = await getDoc(lcRef)

    if (!docSnap.exists()) {
        return null
     }

    let docTemp = docSnap.data() as LeaveCategory

    let lcTemp = {
        id: docSnap.id,
        detail: docTemp.detail
    }
    return lcTemp
}

export async function updateLeaveCategoryById(id: string, detail: string) {
    await updateDoc(doc(db, "leave_categorys", id), {
        detail: detail
    })
     
}

export async function deleteLeaveCategoryById(id: string) {
     await deleteDoc(doc(db, "leave_categorys", id))
}

//update account
