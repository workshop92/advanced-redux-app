import { createUserWithEmailAndPassword, getAuth, signInWithEmailAndPassword, signOut, UserCredential } from "firebase/auth"
import { doc, getDoc, getFirestore, setDoc, updateDoc } from "firebase/firestore"
import { getDownloadURL, getStorage, ref, uploadString } from "firebase/storage";
import { firebassApp } from './../configs/firebase';
import { Account } from './../types/account.typs';
import { getBase64 } from "../utils/img-to-base64";

const auth = getAuth(firebassApp)
const db = getFirestore()
const storage = getStorage(firebassApp)

export async function registerUser(firstName: string, lastName: string, email: string, password: string): Promise<UserCredential> {
    try {
        //register user to authentication
        const userCredential =  await createUserWithEmailAndPassword(auth, email, password)
        
        //save user profile to firestore
        //doc 1 db,  2 ชื่อตาราง, 3 id
        await setDoc(doc(db, "users", userCredential.user.uid), {
            firstName: firstName,
            lastName: lastName,
            photoUrl: "https://codingthailand.com/site/img/nopic.png",
            role: "member"
        })

        return userCredential
    } catch (error) {
        throw error;
    }
}

export async function login(email: string, password: string): Promise<UserCredential> {
    try {
        return await signInWithEmailAndPassword(auth, email, password)
    } catch (error) {
        throw error
    }
}

export async function logout(): Promise<void> {
    await signOut(auth)
}


export async function getCurrentAccount(userId: string): Promise<Account | null> {
    const accountRef = doc(db, "users", userId)
    const docSnap = await getDoc(accountRef)
    if (!docSnap.exists()) {
        return null
    }

    let accTmp = docSnap.data() as Account
    let acc = {
        userId: userId,
        ...accTmp
    }
    return acc    
}


//update account

export async function updateAccount(userId: string, acc: Account): Promise<void> {
    await updateDoc(doc(db, "users", userId), {
        firstName: acc.firstName,
        lastName: acc.lastName
    })
}

//upload image to storage
export async function uploadImageAndUpdatePhotoURL(userId: string, picture: FileList) {
    const base64Image = await getBase64(picture)

    const storageRef = ref(storage, 'user/'+ userId + '/avatar')

    //upload data_url
    await uploadString(storageRef, base64Image, 'data_url') // return snapshot 

    //get image url from server 
    const imageUrl =  await getDownloadURL(storageRef)

    await updateDoc(doc(db, "users", userId), {
        photoUrl: imageUrl
    }) 

}