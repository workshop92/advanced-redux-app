import React, { useEffect, useState } from 'react'
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { firebassApp } from './../configs/firebase';
import { Navigate } from 'react-router-dom';
import { useAppSelector } from '../redux-toolkit/hooks';
import { selectAuthState } from './../redux-toolkit/auth/auth-slice';
import { useAppDispatch } from './../redux-toolkit/hooks';
import { getCurrentAccountThunk } from './../redux-toolkit/auth/auth-thunk';
import { CircularProgress } from '@mui/material';
import { useNavigate } from 'react-router-dom';

type AdminGuardProsType = {
    children: React.ReactNode
}

const AdminGuard = (props: AdminGuardProsType) => {
    const auth = getAuth(firebassApp)
    const navigate = useNavigate()
    const { account, isAuthLoading } = useAppSelector(selectAuthState)
    const dispatch = useAppDispatch()

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                dispatch(getCurrentAccountThunk(user.uid))
            } else {
                //
                navigate('/login')
            }
        })
        return () => unsubscribe()
    }, []);

    if (isAuthLoading === true) return <CircularProgress />

    if (account?.role !== "admin") {
        return <Navigate to="../permission-denied" />
    }

    return  <>{props.children}</>
}

export default AdminGuard