import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import YupPassword from 'yup-password'
YupPassword(yup) // extend yup
import { LoadingButton } from '@mui/lab';
import toast from "react-hot-toast"
import { login } from "../services/auth.service";
import { useAppDispatch } from "../redux-toolkit/hooks";
import { getCurrentAccountThunk } from './../redux-toolkit/auth/auth-thunk';



export default function LoginPage() {
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const schema = yup.object().shape({
        email: yup.string().required('ป้อนอีเมลล์ด้วย').email('รูปแบบอีเมลล์ไม่ถูกต้อง'),
        password: yup.string().required('ป้อนรหัสผ่านด้วย')
            .min(6, 'รหัสผ่านต้องอย่างน้อย 6 ตัวอักษรขึ้นไป')
            .minUppercase(1, 'ต้องมีตัวอักษรตัวใหญ่อย่างน้อย 1 ตัวขึ้นไป')
            .minSymbols(1, 'ต้องมีอักขระพิเศษอย่างน้อย 1 ตัวขึ้นไป'),
    })
    type FormData = yup.InferType<typeof schema>;

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<FormData>({
        resolver: yupResolver(schema),
        mode: 'all'
    });


    const onSubmit = async (data: FormData) => {
        // console.log(data)
        try {
          const userCredential = await login(data.email, data.password!)
          if (userCredential.user != null) {
            dispatch(getCurrentAccountThunk(userCredential.user.uid))
            toast.success("เข้าสู่ระบบสำเร็จ")
            navigate('/dashboard')
          }
        } catch (error: any) {
          if (error.code === "auth/wrong-password") {
            toast.error("รหัสผ่านไม่ถูกต้อง !!!!!!!")
          }  
          else if (error.code === "auth/user-not-found") {
            toast.error("ไม่พบผู้ใช้นี้ในระบบ !!!!!!!")
          }else{
            toast.error("ไม่สามารถทำรายการได้ กรุณาติดต่อ Admin !!!!!!!")
          }
        }
    };


  return (
    <>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            ลงทเบียน
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit(onSubmit)}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                    {...register('email')}
                    error={errors.email ? true : false}
                    helperText={errors.email && errors.email.message}
                    fullWidth
                    label="Email Address"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                    {...register('password')}
                    error={errors.password ? true : false}
                    helperText={errors.password && errors.password.message}
                    fullWidth
                    label="Password"
                    type="password"
                />
              </Grid>
            </Grid>
            <LoadingButton 
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              loading={isSubmitting}
            >
              เข้าสู่ระบบ
            </LoadingButton >
            <Grid container justifyContent="space-between">
              <Grid item>
                <RouterLink to="/">กลับหน้าหลัก</RouterLink>
              </Grid>

            </Grid>
          </Box>
        </Box>
      </Container>
    </>
  );
}
