import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import YupPassword from 'yup-password'
YupPassword(yup) // extend yup
import { LoadingButton } from '@mui/lab';
import toast from "react-hot-toast"


import { registerUser } from "../services/auth.service";

export default function RegisterPage() {
    const navigate = useNavigate()
    const schema = yup.object().shape({
        firstName: yup.string().required('ป้อนข้อมูลชื่อด้วย'),
        lastName: yup.string().required('ป้อนข้อมูลนามสกุลด้วย'),
        email: yup.string().required('ป้อนอีเมลล์ด้วย').email('รูปแบบอีเมลล์ไม่ถูกต้อง'),
        password: yup.string().required('ป้อนรหัสผ่านด้วย')
            .min(6, 'รหัสผ่านต้องอย่างน้อย 6 ตัวอักษรขึ้นไป')
            .minUppercase(1, 'ต้องมีตัวอักษรตัวใหญ่อย่างน้อย 1 ตัวขึ้นไป')
            .minSymbols(1, 'ต้องมีอักขระพิเศษอย่างน้อย 1 ตัวขึ้นไป'),
    })
    type FormData = yup.InferType<typeof schema>;

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<FormData>({
        resolver: yupResolver(schema),
        mode: 'all'
    });

    // if (isSubmitting) {
    //   return  <c
    // }

    const onSubmit = async (data: FormData) => {
        // console.log(data)
        try {
          const userCredential = await registerUser(data.firstName, data.lastName, data.email, data.password!)
          if (userCredential.user != null) {
            toast.success("ลงทะเบียนสำเร็จ")
            navigate('/')
          }
        } catch (error: any) {
          if (error.code === "auth/email-already-in-use") {
            toast.error("มีผู้ใช้งานอีเมลล์นี้แล้วในระบบ !!!!!!!")
          }else{
            toast.error("ไม่สามารถทำรายการได้ กรุณาติดต่อ Admin !!!!!!!")
          }
        }
    };


  return (
    <>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            ลงทเบียน
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit(onSubmit)}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                    {...register('firstName')}
                    error={errors.firstName ? true : false}
                    helperText={errors.firstName && errors.firstName.message}
                    fullWidth
                    label="First Name"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                    {...register('lastName')}
                    error={errors.lastName ? true : false}
                    helperText={errors.lastName && errors.lastName.message}
                    fullWidth
                    label="Last Name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                    {...register('email')}
                    error={errors.email ? true : false}
                    helperText={errors.email && errors.email.message}
                    fullWidth
                    label="Email Address"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                    {...register('password')}
                    error={errors.password ? true : false}
                    helperText={errors.password && errors.password.message}
                    fullWidth
                    label="Password"
                    type="password"
                />
              </Grid>
            </Grid>
            <LoadingButton 
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              loading={isSubmitting}
            >
              ลงทะเบียน
            </LoadingButton >
            <Grid container justifyContent="space-between">
              <Grid item>
                <RouterLink to="/">กลับหน้าหลัก</RouterLink>
              </Grid>
              <Grid item>
                <RouterLink to="#">Already have an account? Log In</RouterLink>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </>
  );
}
