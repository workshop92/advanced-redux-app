import Avatar from "@mui/material/Avatar";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import YupPassword from 'yup-password'
YupPassword(yup) // extend yup
import { LoadingButton } from '@mui/lab';
import toast from "react-hot-toast"
import { useAccount } from './../../hooks/use-account';
import { useAppDispatch } from './../../redux-toolkit/hooks';
import { updateAccountThunk, getCurrentAccountThunk } from './../../redux-toolkit/auth/auth-thunk';
import { IconButton } from "@mui/material";
import { PhotoCamera } from "@mui/icons-material";

export default function DEditProfile() {
    const navigate = useNavigate()
    const { account } = useAccount()
    const dispatch = useAppDispatch()

    const schema = yup.object().shape({
        firstName: yup.string().required('ป้อนข้อมูลชื่อด้วย'),
        lastName: yup.string().required('ป้อนข้อมูลนามสกุลด้วย'),
        picture: yup.mixed().test("fileSize", "File ไม่เกิน 5MB", (value: any) => {
          if (value.length > 0) {
            if (value[0].size > 5*1024*1024) {
              return false
            }
          }
          return true
        })
    })
    type FormData = yup.InferType<typeof schema>;

    const { register, handleSubmit, formState: { errors, isSubmitting } } = useForm<FormData>({
        resolver: yupResolver(schema),
        mode: 'all'
    });


    const onSubmit = async (data: FormData) => {
        try {

            await dispatch(updateAccountThunk({
                userId: account?.userId,
                acc: {
                    firstName: data.firstName,
                    lastName: data.lastName,
                },
                picture: data.picture
            }))

            await dispatch(getCurrentAccountThunk(account?.userId))

            toast.success("แก้ไขข้อมูลส่วนตัวสำเร็จ")
            navigate("../")
        
        } catch (error: any) {
            toast.error(error)
        }
    };


  return (
    <>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ width: 100, height: 100}} src={account?.photoUrl}/>
            <IconButton color="primary" aria-label="upload picture" component="label">
                <input hidden accept="image/*" type="file" {...register('picture')}/>
                <PhotoCamera fontSize="large" />
                <Typography component="h1" variant="h4" color='red'>
                  {errors.picture && errors.picture.message}
                </Typography>
            </IconButton>
          <Typography component="h1" variant="h5">
            แก้ไขข้อมูลส่วนตัว
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit(onSubmit)}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                    {...register('firstName')}
                    error={errors.firstName ? true : false}
                    helperText={errors.firstName && errors.firstName.message}
                    fullWidth
                    label="First Name"
                    defaultValue={account?.firstName}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                    {...register('lastName')}
                    error={errors.lastName ? true : false}
                    helperText={errors.lastName && errors.lastName.message}
                    fullWidth
                    label="Last Name"
                    defaultValue={account?.lastName}
                />
              </Grid>
              
            </Grid>

            <LoadingButton 
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              loading={isSubmitting}
            >
              บันทึก
            </LoadingButton >
            
          </Box>
        </Box>
      </Container>
    </>
  );
}
