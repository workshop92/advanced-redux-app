import React, { useEffect } from 'react'
import { Link as RouterLink, useNavigate } from 'react-router-dom'
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import {
  DataGrid,
  GridActionsCellItem,
  GridRowId,
  GridColDef,
  GridColumnHeaderParams,
} from '@mui/x-data-grid';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

import { withAdminGuard } from '../../../hocs/with-admin-guard';
import { Box, Button, Container, Divider } from '@mui/material';
import { useAppDispatch, useAppSelector } from '../../../redux-toolkit/hooks';
import { deleteLeaveCategoryThunk, getLeaveCategoryThunk } from '../../../redux-toolkit/leave-category/leave-category-thunk';
import { selectLeaveCategoryState } from '../../../redux-toolkit/leave-category/leave-category-slice';
import { LeaveCategory } from '../../../types/leave-category.type';
import { RenderCellExpand } from '../../../hooks/use-renderCellExpand';



const DManageLeaveIndex = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { data, isLoading, errorMsg } = useAppSelector(selectLeaveCategoryState)

  useEffect(() => {
    // getLeaveCategory()
    dispatch(getLeaveCategoryThunk())
  }, [])

  const deleteLeaveCategory = React.useCallback(
    (id: GridRowId) => async () => {
      // console.log(id)
      if (confirm("แน่ใจว่าต้องากรลบข้อมูลนี้ใช่หรือไม่!!!!!!!")){
        await dispatch(deleteLeaveCategoryThunk(id.toString()))
        dispatch(getLeaveCategoryThunk())
      }
    },
    [],
  );

  const editLeaveCategory = React.useCallback(
    (id: GridRowId) => () => {
      // console.log(id)
      navigate("./" + id + "/edit")
    },
    [],
  );


  const columns = React.useMemo<GridColDef<LeaveCategory>[]>(
    () => [
      { 
        field: 'id',
        width: 300,
        renderHeader: (params: GridColumnHeaderParams) => (
          <Box fontSize={25}>
            ID
          </Box>
        ),
      },
      { 
        field: 'detail', headerName: "ประเภทการลา", width: 500, 
      },
      {
        field: 'actions',
        type: 'actions',
        width: 120,
        headerName: 'Action',
        getActions: (params) => [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            onClick={editLeaveCategory(params.id)}
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={deleteLeaveCategory(params.id)}
          />,
          
        ],
      },
    ],
    [deleteLeaveCategory, editLeaveCategory],
  );
  return (
    <>
    <Container>
    <Button 
      variant="contained"
      component={RouterLink} to="/dashboard/manage-leave/create"
      >เพิ่มข้อมูล</Button>
    <Divider sx={{my: 2}}/>

    <TableContainer component={Paper}>
      {data &&
        <DataGrid 
          columns={columns} 
          rows={data!} 
          // autoPageSize={true}
          disableColumnFilter
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 5
              }
            }
          }}
          pageSizeOptions={[5]}
          loading={isLoading}
          />
      }
      
    </TableContainer>
    </Container>
    </>
  )
}

export default withAdminGuard(DManageLeaveIndex)