import React from 'react';
import { useAccount } from './../../hooks/use-account';

const DHome = () => {
    const { account } = useAccount()
    const password = React.useId()
    return (
        <>
            <div>ยินดีต้อนรับ { account?.firstName } { account?.lastName }</div>
            <div>{account?.photoUrl}</div>
        </>
    );
}

export default DHome;