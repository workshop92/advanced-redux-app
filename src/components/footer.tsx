import { Box } from '@mui/material';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';

export const Appfooter = (props: any) => {
  return (
        <>
            <Box sx={{ bgcolor: 'gray.200', p: 6 }} component="footer">
                <Typography variant="body2" color="text.secondary" align="center" {...props}>
                    {'Copyright © '}
                <Link color="inherit" href="https://mui.com/">
                    ระบบลาออนไลน์
                </Link>{' '}
                {new Date().getFullYear()}
                {'.'}
                </Typography>
            </Box>
        </>
  )
}