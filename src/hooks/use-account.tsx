import { useNavigate } from "react-router-dom";
import { selectAuthState } from "../redux-toolkit/auth/auth-slice"
import { useAppSelector } from "../redux-toolkit/hooks"
import { logout } from "../services/auth.service";


export const useAccount = () => {
    const { account } = useAppSelector(selectAuthState)
    const navigate = useNavigate()

    const handleLogout = async () => {
        await logout()
        navigate("/")
      };

    return { account, handleLogout }
}