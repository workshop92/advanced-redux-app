import { createTheme } from '@mui/material/styles';
import { red } from '@mui/material/colors';

// A custom theme for this app
const theme = createTheme({
  palette: {
    primary: {
      main: '#556cd6',
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      main: red.A400,
    },
  },
});

const darkTheme = createTheme({
  palette: {
    // type: "dark",
    background: {
      default: "hsl(230, 17%, 14%)"
    }
  }
});

const lightTheme = createTheme({
  palette: {
    // type: "light",
    background: {
      default: "hsl(0, 0%, 100%)"
    }
  }
});
let mode = "darkx"

const selectedTheme = mode === "dark" ? darkTheme : lightTheme;

export default selectedTheme;