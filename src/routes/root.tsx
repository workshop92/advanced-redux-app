import { createBrowserRouter } from "react-router-dom";
import HomePage from "../pages/home-page";
import RegisterPage from "../pages/register-page";
import routerDashboard from "./dashboard";
import LoginPage from './../pages/login-page';
import routeTest from "./test";


const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />
  },
  {
    path: "/register",
    element: <RegisterPage />
  },
  {
    path: "/login",
    element: <LoginPage />
  },
  ...routerDashboard,
  ...routeTest
  
]);

export default router;