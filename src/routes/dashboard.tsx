import { RouteObject } from "react-router-dom";
import DEditProfile from "../pages/dashboard/d-edit-profile";
import DHome from "../pages/dashboard/d-home";
import DLayout from "../pages/dashboard/d-layout";
import DLeave from "../pages/dashboard/d-leave";
import DManageLeaveIndex from "../pages/dashboard/d-manage-leave/index";
import PermissionDenied from "../pages/dashboard/permission-denied";
import DmanageLeaveCreate from "../pages/dashboard/d-manage-leave/create";
import DmanageLeaveEdit, { dManageLeaveEditLoader } from "../pages/dashboard/d-manage-leave/edit";


const routeDashboard: RouteObject[] = [
    {
        path: "/dashboard",
        element: <DLayout />,
        children: [
            {
                path: "",// localhost:4000/dashboard
                element: <DHome />
            },
            {
                path: "request-for-leave",
                element: <DLeave />
            },
            {
                path: "manage-leave",
                element: <DManageLeaveIndex />
            },
            {
                path: "manage-leave/create",
                element: <DmanageLeaveCreate />
            },
            {
                path: "manage-leave/:id/edit",
                loader: dManageLeaveEditLoader,
                element: <DmanageLeaveEdit />
            },
            {
                path: "edit-profile",
                element: <DEditProfile />
            },
            {
                path: "permission-denied",
                element: <PermissionDenied />
            },
          
        ],

    }
]


export default routeDashboard;
