import { RouteObject } from "react-router-dom";
import TestLayout from "../pages/test/t-layout";
import ThomePage from "../pages/test/t-home-page";


const routeTest: RouteObject[] = [
    {
        path: "/test",
        element: <TestLayout />,
        children: [
            {
                path: "",// localhost:4000/dashboard
                element: <ThomePage />
            },

        ],

    }
]


export default routeTest;
