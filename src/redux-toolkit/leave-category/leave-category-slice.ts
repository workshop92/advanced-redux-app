import { createSlice } from "@reduxjs/toolkit"
import { RootState } from "../store"
import { LeaveCategory } from "../../types/leave-category.type"
import { getLeaveCategoryThunk } from "./leave-category-thunk"



export type LeaveCategoryState = {
    isLoading: boolean
    data: LeaveCategory[] | null
    errorMsg: any | null
}

const initialState: LeaveCategoryState = {
    isLoading: true,
    data: null,
    errorMsg: null
}

export const leaveCategorySlice = createSlice({
    name: "leaveCategory",
    initialState: initialState,
    reducers: {},
    extraReducers(builder) {
        builder.addCase(getLeaveCategoryThunk.fulfilled, (state, action) => {
            state.isLoading = false
            state.data = action.payload
        })
    },
})

export const selectLeaveCategoryState = (state: RootState) => state.leaveCategoryState

export default leaveCategorySlice.reducer
