import { createAsyncThunk } from "@reduxjs/toolkit";
import { createLeaveCategory, deleteLeaveCategoryById, getLeaveCategory } from "../../services/leave-category.service";

export const createLeaveCategoryThunk = createAsyncThunk(
    "lc/createLeaveCategoryThunk",
    async (detail: string) => {
        try {
            await createLeaveCategory(detail)
        } catch (error: any) {
            throw error;
        }
    }
)

export const getLeaveCategoryThunk = createAsyncThunk(
    "lc/getLeaveCategoryThunk",
    async () => {
        try {
            const lc = await getLeaveCategory()
            return lc
        } catch (error: any) {
            throw error;
        }
    }
)


export const deleteLeaveCategoryThunk = createAsyncThunk(
    "lc/deleteLeaveCategoryThunk",
    async (id: string) => {
        try {
            await deleteLeaveCategoryById(id)
        } catch (error: any) {
            throw error;
        }
    }
)
